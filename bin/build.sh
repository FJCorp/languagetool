#!/bin/sh

IMAGE_NAME=$REGISTRY/languagetool:$CI_PIPELINE_ID

buildah bud --squash -t $IMAGE_NAME .

buildah login -u AWS -p $REGISTRY_PASSWORD $REGISTRY

buildah push $IMAGE_NAME
